# Custom UI Hello World

This Custom UI React app builds it's resources within the Forge App package located at `../forge`.

## Quick start

- Build your app:
```
yarn build
```

- Head to the `../forge` package and deploy your app:
```
forge deploy
```

- Install your app in an Atlassian site by running:
```
forge install
```

### Notes
- Check the `../forge` package for more notes & indications.
