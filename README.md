# Forge & Custom UI in Lerna Monorepo

This project contains a Forge app written in Javascript that displays `Hello World!` in a Confluence content byline item. 

The Custom UI React app is a package in the monorepo, and the Forge App is a different package. The Custom UI React app builds it's resources within the Forge App package.

## Quick start
This project uses Yarn package manager instead of Node, so please, do not run `npm install` in this project ever, unless you need it to install Yarn globally.

- Install all the dependencies and bootstrap Lerna
```
yarn bootstrap
```

- Install the Forge App (includes Custom UI resource bundling and Forge App deployment)
```
yarn forge-install
```

- Deploy the Forge App (includes Custom UI resource bundling)
```
yarn deploy
```

- Start Forge Tunnel (doesn't work ATM)
```
yarn tunnel
```
